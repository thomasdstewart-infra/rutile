#!/bin/bash -ux

export BUCKET="images"
export DEBIAN_RELEASE="bookworm"
export DEBIAN_VERSION="12"

# shellcheck disable=SC2154
export OCI_CLI_USER="$TF_VAR_user_ocid"
# shellcheck disable=SC2154
export OCI_CLI_REGION="$TF_VAR_region"
# shellcheck disable=SC2154
export OCI_CLI_FINGERPRINT="$TF_VAR_fingerprint"
# shellcheck disable=SC2154
OCI_CLI_KEY_CONTENT="$(echo "$TF_VAR_private_key" | base64 -d)"
export OCI_CLI_KEY_CONTENT
# shellcheck disable=SC2154
export OCI_CLI_TENANCY="$TF_VAR_tenancy_ocid"

export IMAGE_BASE_URL="https://cloud.debian.org/images/cloud/$DEBIAN_RELEASE"

IMAGE_VERSION="$(curl -sL "$IMAGE_BASE_URL" | sed 's/.* href="\([0-9][0-9-]*\).*/\1/' | grep ^20 | tail -1)"
export IMAGE_VERSION

export IMAGE_NAME="debian-$DEBIAN_VERSION-genericcloud-amd64-$IMAGE_VERSION"
export IMAGE_NAME_ARM="debian-$DEBIAN_VERSION-generic-arm64-$IMAGE_VERSION"

export IMAGE_URL="$IMAGE_BASE_URL/$IMAGE_VERSION/$IMAGE_NAME.qcow2"
export IMAGE_URL_ARM="$IMAGE_BASE_URL/$IMAGE_VERSION/$IMAGE_NAME_ARM.qcow2"

IMAGE_SIZE=$(oci os object list --bucket-name "$BUCKET" | jq -r '.data[] | select(."name"==env.IMAGE_NAME).size')
if [ "$IMAGE_SIZE" = "" ]; then
        oci os object list --bucket-name "$BUCKET" | jq -r '.data[].name' | while read -r OBJECT; do
                oci os object delete --bucket-name "$BUCKET" --name "$OBJECT" --force
        done
        oci os bucket delete --name "$BUCKET" --force || true
        # shellcheck disable=SC2154
        oci os bucket create --name "$BUCKET" --compartment-id "$TF_VAR_compartment_id"
        curl -L "$IMAGE_URL"     | oci os object put --bucket-name "$BUCKET" --name "$IMAGE_NAME"     --file - --force
        curl -L "$IMAGE_URL_ARM" | oci os object put --bucket-name "$BUCKET" --name "$IMAGE_NAME_ARM" --file - --force
fi

echo "TF_VAR_image_name=$IMAGE_NAME" > images.env
echo "TF_VAR_image_name_arm=$IMAGE_NAME_ARM" >> images.env
