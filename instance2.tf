resource "oci_core_instance" "instance2" {
  display_name        = "rutile2"
  availability_domain = "NODj:EU-ZURICH-1-AD-1"
  compartment_id      = var.compartment_id
  shape               = "VM.Standard.E2.1.Micro"
  source_details {
    source_type             = "image"
    source_id               = oci_core_image.debian_image.id
    boot_volume_size_in_gbs = "50"
  }
  create_vnic_details {
    subnet_id        = oci_core_subnet.subnet.id
    assign_public_ip = "true"
    nsg_ids          = [oci_core_network_security_group.nsg.id]
  }
  metadata = {
    "ssh_authorized_keys" = base64decode(var.public_key)
  }
  lifecycle {
    replace_triggered_by = [
      terraform_data.debian_image,
    ]
  }
}
