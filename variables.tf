variable "tenancy_ocid" {
  sensitive = true
}
variable "user_ocid" {
  sensitive = true
}
variable "fingerprint" {
  sensitive = true
}
variable "private_key" {
  sensitive = true
}
variable "region" {
  sensitive = true
}
variable "compartment_id" {
  sensitive = true
}
variable "namespace_name" {
  sensitive = true
}
variable "public_key" {
  sensitive = true
}

variable "base_name" {
  type    = string
  default = "rutile"
}

variable "image_name" {}
variable "image_name_arm" {}
