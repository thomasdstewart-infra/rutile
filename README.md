# Rutile

[![pipeline status](https://gitlab.com/thomasdstewart-infra/rutile/badges/main/pipeline.svg)](https://gitlab.com/thomasdstewart-infra/rutile/-/commits/main)

This interacts with Oracle Cloud to create infra, using IaC.

## Files
 * .gitlab-ci.yml - Gitlab CI/CD script
 * uploadimages.sh - uploads images to oracle cloud infra
 * *.tf - Terraform templates

## Setup

Once Signed up to Oracle Cloud, create an ssh key for the api services, install this pub key under the user. This will reveal most of these environment vars needed for the GitLab CI/CD vars (ssh keys are base64):

## Local running
Clone the repo
```
cd ~/src
git clone https://gitlab.com/thomasdstewart-infra/rutile.git
```

Set env vars
```
export TF_VAR_user_ocid=user id, eg ocid1.user.oc1..aaaa
export TF_VAR_region=region eg eu-zurich-1
export TF_VAR_tenancy_ocid=tenancy id, eg ocid1.tenancy.oc1..aaaa
export TF_VAR_compartment_id=compartment id, eg ocid1.tenancy.oc1..aaaa
export TF_VAR_private_key=api ssh key, eg -----BEGIN RSA PRIVATE KE
export TF_VAR_fingerprint=fingerprint of key, eg 01:23:45
export TF_VAR_public_key=pub key to be installed to instance, eg ssh-ed25519 AAAA
export TF_VAR_namespace_name=object namespace, eg abc (oci os ns get)
```

Upload images
```
docker run --rm -it -v ~/src/rutile:/rutile --entrypoint "" -e TF_VAR_user_ocid=$TF_VAR_user_ocid -e TF_VAR_region=$TF_VAR_region -e TF_VAR_tenancy_ocid=$TF_VAR_tenancy_ocid -e TF_VAR_compartment_id=$TF_VAR_compartment_id -e TF_VAR_private_key=$TF_VAR_private_key -e TF_VAR_fingerprint=$TF_VAR_fingerprint -e TF_VAR_public_key=$TF_VAR_public_key -e TF_VAR_namespace_name=$TF_VAR_namespace_name ghcr.io/oracle/oci-cli:latest cd /rutile; ./uploadimages.sh
```

Run terraform
```
docker run --rm -it -v ~/src/rutile:/rutile -e TF_VAR_user_ocid=$TF_VAR_user_ocid -e TF_VAR_region=$TF_VAR_region -e TF_VAR_tenancy_ocid=$TF_VAR_tenancy_ocid -e TF_VAR_compartment_id=$TF_VAR_compartment_id -e TF_VAR_private_key=$TF_VAR_private_key -e TF_VAR_fingerprint=$TF_VAR_fingerprint -e TF_VAR_public_key=$TF_VAR_public_key -e TF_VAR_namespace_name=$TF_VAR_namespace_name registry.gitlab.com/gitlab-org/terraform-images/stable:latest sh
cd /rutile
export $(cat images.env | xargs)
terraform init
terraform validate
terraform plan -input=false -detailed-exitcode -out .tfplan
terraform apply -auto-approve -input=false .tfplan
terraform output -json > output.json
#terraform destroy
```
