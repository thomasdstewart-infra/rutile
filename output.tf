output "rutile1" {
  value = oci_core_instance.instance1.public_ip
}

output "rutile2" {
  value = oci_core_instance.instance2.public_ip
}

output "rutile3" {
  value = oci_core_instance.instance3.public_ip
}
