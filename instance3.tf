resource "oci_core_instance" "instance3" {
  display_name        = "rutile3"
  availability_domain = "NODj:EU-ZURICH-1-AD-1"
  compartment_id      = var.compartment_id
  shape               = "VM.Standard.A1.Flex"
  shape_config {
    memory_in_gbs = "24"
    ocpus         = "4"
  }
  #  launch_options {
  #    firmware         = "UEFI_64"
  #    boot_volume_type = "PARAVIRTUALIZED"
  #    network_type     = "PARAVIRTUALIZED"
  #  }
  source_details {
    source_type             = "image"
    source_id               = oci_core_image.debian_image_arm.id
    boot_volume_size_in_gbs = "60"
  }
  create_vnic_details {
    subnet_id        = oci_core_subnet.subnet.id
    assign_public_ip = "true"
    nsg_ids          = [oci_core_network_security_group.nsg.id]
  }
  metadata = {
    "ssh_authorized_keys" = base64decode(var.public_key)
  }
  lifecycle {
    replace_triggered_by = [
      terraform_data.debian_image_arm,
    ]
  }
}
