# https://github.com/oracle/terraform-provider-oci/issues/2052

data "oci_core_compute_global_image_capability_schemas_versions" "compute_global_image_capability_schemas_versions" {
  compute_global_image_capability_schema_id = data.oci_core_compute_global_image_capability_schema.compute_global_image_capability_schema.id
}

data "oci_core_compute_global_image_capability_schema" "compute_global_image_capability_schema" {
  compute_global_image_capability_schema_id = data.oci_core_compute_global_image_capability_schemas.compute_global_image_capability_schemas.compute_global_image_capability_schemas[0].id
}

data "oci_core_compute_global_image_capability_schemas" "compute_global_image_capability_schemas" {
}

resource "oci_core_compute_image_capability_schema" "custom_image_capability_schema" {
  compartment_id                                      = var.compartment_id
  compute_global_image_capability_schema_version_name = data.oci_core_compute_global_image_capability_schemas_versions.compute_global_image_capability_schemas_versions.compute_global_image_capability_schema_versions[1].name
  display_name                                        = "Debian-12-arm64-Capability-Schema"
  image_id                                            = oci_core_image.debian_image_arm.id

  schema_data = {
    "Compute.AMD_SecureEncryptedVirtualization" = jsonencode(
      {
        defaultValue   = false
        descriptorType = "boolean"
        source         = "IMAGE"
      }
    )
    "Compute.Firmware" = jsonencode(
      {
        defaultValue   = "UEFI_64"
        descriptorType = "enumstring"
        source         = "IMAGE"
        values = [
          "BIOS",
          "UEFI_64",
        ]
      }
    )
    "Compute.LaunchMode" = jsonencode(
      {
        defaultValue   = "PARAVIRTUALIZED"
        descriptorType = "enumstring"
        source         = "IMAGE"
        values = [
          "NATIVE",
          "EMULATED",
          "VDPA",
          "PARAVIRTUALIZED",
          "CUSTOM",
        ]
      }
    )
    "Compute.SecureBoot" = jsonencode(
      {
        defaultValue   = false
        descriptorType = "boolean"
        source         = "IMAGE"
      }
    )
    "Network.AttachmentType" = jsonencode(
      {
        defaultValue   = "PARAVIRTUALIZED"
        descriptorType = "enumstring"
        source         = "IMAGE"
        values = [
          "VFIO",
          "PARAVIRTUALIZED",
          "E1000",
          "VDPA",
        ]
      }
    )
    "Network.IPv6Only" = jsonencode(
      {
        defaultValue   = false
        descriptorType = "boolean"
        source         = "IMAGE"
      }
    )
    "Storage.BootVolumeType" = jsonencode(
      {
        defaultValue   = "PARAVIRTUALIZED"
        descriptorType = "enumstring"
        source         = "IMAGE"
        values = [
          "ISCSI",
          "PARAVIRTUALIZED",
          "SCSI",
          "IDE",
          "NVME",
        ]
      }
    )
    "Storage.ConsistentVolumeNaming" = jsonencode(
      {
        defaultValue   = true
        descriptorType = "boolean"
        source         = "IMAGE"
      }
    )
    "Storage.Iscsi.MultipathDeviceSupported" = jsonencode(
      {
        defaultValue   = false
        descriptorType = "boolean"
        source         = "IMAGE"
      }
    )
    "Storage.LocalDataVolumeType" = jsonencode(
      {
        defaultValue   = "PARAVIRTUALIZED"
        descriptorType = "enumstring"
        source         = "IMAGE"
        values = [
          "ISCSI",
          "PARAVIRTUALIZED",
          "SCSI",
          "IDE",
          "NVME",
        ]
      }
    )
    "Storage.ParaVirtualization.AttachmentVersion" = jsonencode(
      {
        defaultValue   = 2
        descriptorType = "enuminteger"
        source         = "IMAGE"
        values = [
          1,
          2,
        ]
      }
    )
    "Storage.ParaVirtualization.EncryptionInTransit" = jsonencode(
      {
        defaultValue   = true
        descriptorType = "boolean"
        source         = "IMAGE"
      }
    )
    "Storage.RemoteDataVolumeType" = jsonencode(
      {
        defaultValue   = "PARAVIRTUALIZED"
        descriptorType = "enumstring"
        source         = "IMAGE"
        values = [
          "ISCSI",
          "PARAVIRTUALIZED",
          "SCSI",
          "IDE",
          "NVME",
        ]
      }
    )
  }

}
