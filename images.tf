resource "oci_core_image" "debian_image" {
  compartment_id = var.compartment_id

  display_name = var.image_name
  launch_mode  = "PARAVIRTUALIZED"

  image_source_details {
    source_type    = "objectStorageTuple"
    bucket_name    = "images"
    namespace_name = var.namespace_name
    object_name    = var.image_name

    operating_system         = "Debian"
    operating_system_version = "12"
    source_image_type        = "QCOW2"
  }
}

resource "oci_core_shape_management" "debian_image_shape" {
  compartment_id = var.compartment_id
  image_id       = oci_core_image.debian_image.id
  shape_name     = "VM.Standard.E2.1.Micro"
}

resource "oci_core_image" "debian_image_arm" {
  compartment_id = var.compartment_id

  display_name = var.image_name_arm
  launch_mode  = "PARAVIRTUALIZED"

  image_source_details {
    source_type    = "objectStorageTuple"
    bucket_name    = "images"
    namespace_name = var.namespace_name
    object_name    = var.image_name_arm

    operating_system         = "Debian"
    operating_system_version = "12"
    source_image_type        = "QCOW2"
  }
}

resource "oci_core_shape_management" "debian_image_arm_shape" {
  compartment_id = var.compartment_id
  image_id       = oci_core_image.debian_image_arm.id
  shape_name     = "VM.Standard.A1.Flex"
}

#https://github.com/oracle/terraform-provider-oci/issues/2133
resource "terraform_data" "debian_image" {
  input = oci_core_image.debian_image
}

resource "terraform_data" "debian_image_arm" {
  input = oci_core_image.debian_image_arm
}
