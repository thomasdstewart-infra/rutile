terraform {
  required_version = ">= 1.5.7"
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = ">= 5.0.0, < 6.0.0"
    }
  }


  backend "http" {
  }
}

provider "oci" {
  tenancy_ocid = var.tenancy_ocid
  user_ocid    = var.user_ocid
  fingerprint  = var.fingerprint
  private_key  = base64decode(var.private_key)
  region       = var.region
}

#https://github.com/oracle/terraform-provider-oci/blob/master/examples/always_free/main.tf
